<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_dashboard extends CI_Model {

	
	function months() {
	return array('Jan','Peb','Mar','Apr','Mei','Jun','Jul','Agu','Sep','Okt','Nop','Des');
			
    }

	
	function jcustomer()
	{
		$unit = $this->session->userdata('unit');				 
		$this->db->select('COALESCE(COUNT(idtr),0) as jml');
		$this->db->from( 'tbl_pasien' );		
		$this->db->where('koders',$unit);
		$query = $this->db->get();
		// return $query->num_rows();
		return $query->result();		
	}
	
	function dsaset()
	{
	   //return $this->db->query('select sum(hargabeli*stok) as total from inv_barang')->row()->total;
	   return 0;
	}
	
	function dshutang()
	{
	   $unit = $this->session->userdata('unit');				 	 
	   return $this->db->query("select sum(totaltagihan-totalbayar) as total from tbl_apoap where koders = '$unit'")->row()->total;
	}
	
	function dspiutang()
	{
	   return 0;
	   
	}
	
	function report(){
		$tahun = date('Y');
		$q ="
		    select 'Jan' as bulan, count(*) as jumlah from tbl_regist where year(tglmasuk) = '$tahun' and month(tglmasuk)=1
			union
			select 'Peb' as bulan, count(*) as jumlah from tbl_regist where year(tglmasuk) = '$tahun' and month(tglmasuk)=2
			union
			select 'Mar' as bulan, count(*) as jumlah from tbl_regist where year(tglmasuk) = '$tahun' and month(tglmasuk)=3
			union
			select 'Apr' as bulan, count(*) as jumlah from tbl_regist where year(tglmasuk) = '$tahun' and month(tglmasuk)=4
			union
			select 'Mei' as bulan, count(*) as jumlah from tbl_regist where year(tglmasuk) = '$tahun' and month(tglmasuk)=5
			union
			select 'Jun' as bulan, count(*) as jumlah from tbl_regist where year(tglmasuk) = '$tahun' and month(tglmasuk)=6
			union
			select 'Jul' as bulan, count(*) as jumlah from tbl_regist where year(tglmasuk) = '$tahun' and month(tglmasuk)=7
			union
			select 'Agu' as bulan, count(*) as jumlah from tbl_regist where year(tglmasuk) = '$tahun' and month(tglmasuk)=8
			union
			select 'Sep' as bulan, count(*) as jumlah from tbl_regist where year(tglmasuk) = '$tahun' and month(tglmasuk)=9
			union
			select 'Okt' as bulan, count(*) as jumlah from tbl_regist where year(tglmasuk) = '$tahun' and month(tglmasuk)=10
			union
			select 'Nop' as bulan, count(*) as jumlah from tbl_regist where year(tglmasuk) = '$tahun' and month(tglmasuk)=11
			union
			select 'Des' as bulan, count(*) as jumlah from tbl_regist where year(tglmasuk) = '$tahun' and month(tglmasuk)=12";

			
			  
        $query = $this->db->query($q);
         
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
		
		
    }

    

}
?>