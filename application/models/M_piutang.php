<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_piutang extends CI_Model {

	var $table = 'tbl_pap';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();	
	}

	public function getTotalPiutang($unit){
		$query = "
			SELECT COALESCE(SUM(jumlahhutang-jumlahbayar),0) AS total
            FROM tbl_pap
            WHERE koders = '$unit'
		";
		$qry = $this->db->query($query)->num_rows();

		if($qry > 0){
			return $this->db->query($query)->result();
		} else {
			return false;
		}
	}

    
	public function getTotalAsuransi($unit){
		$query = "
			SELECT COALESCE(SUM(jumlahhutang-jumlahbayar),0) AS asuransi
            FROM tbl_pap
            WHERE cust_id <> 'BPJS' AND koders = '$unit'
		";
		$qry = $this->db->query($query)->num_rows();

		if($qry > 0){
			return $this->db->query($query)->result();
		} else {
			return false;
		}
	}

    
	public function getTotalBpjs($unit){
		$query = "
			SELECT COALESCE(SUM(p.jumlahhutang-p.jumlahbayar),0) AS simrs,
                COALESCE(SUM(p.inacbg-p.jumlahbayar),0) AS inacbg
            FROM tbl_pap p INNER JOIN tbl_penjamin pj ON p.cust_id = pj.cust_id
            WHERE p.cust_id = 'BPJS' AND p.koders = '$unit'
		";
		$qry = $this->db->query($query)->num_rows();

		if($qry > 0){
			return $this->db->query($query)->result();
		} else {
			return false;
		}
	}

    public function getDataPiutang($unit, $tahun, $bln){
        
        $query = 
            "
            SELECT p.cust_id, pj.cust_nama, COALESCE(SUM(p.jumlahhutang),0) AS totalpiutang,
                COALESCE(SUM(p.inacbg-p.jumlahbayar),0) AS inacbg, 
				COALESCE(SUM(p.jumlahbayar),0) AS dibayar, 
				COALESCE(SUM(p.jumlahhutang-p.jumlahbayar),0) AS saldopiutang
            FROM tbl_pap p INNER JOIN tbl_penjamin pj ON p.cust_id = pj.cust_id
            WHERE  
                koders = '".$unit."' 
				-- AND    
                -- DATE_FORMAT(tglposting,'%Y-%m') = '".$tahun."-".$bln."'
            GROUP BY p.cust_id
        ";

		$qry = $this->db->query($query)->num_rows();

		if($qry > 0){
			return $this->db->query($query)->result();
		} else {
			return false;
		}
	}
    
 
}