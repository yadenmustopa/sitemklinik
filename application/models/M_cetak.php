<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_cetak extends CI_Model {


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->session->set_userdata('menuapp', '400');
		$this->session->set_userdata('submenuapp', '410');
	}


	function mpdf($form='' ,$uk='' , $judul='',$isi='',$jdlsave='',$lMargin='',$rMargin='',$font=10,$orientasi='',$hal='',$tab='',$tMargin='')
    {

        ini_set("memory_limit", "-1");
        ini_set("MAX_EXECUTION_TIME","-1");
		
        $jam = date("H:i:s");
		if ($hal==''){
		$hal1=1;
		} 
		if($hal!==''){
		$hal1=$hal;
		}
		if ($font==''){
		$size=12;
		}else{
		$size=$font;
		} 

		if ($tMargin=='' ){
			$tMargin=16;
		}
		
		if($lMargin==''){
			$lMargin=15;
		}

		if($rMargin==''){
			$rMargin=15;
		}
        $this->mpdf = new \Mpdf\Mpdf( array(190,236),$size,'',$lMargin,$rMargin,$tMargin);
        $this->mpdf->AddPage($form,$uk);
		// $this->mpdf->SetHeader('SKI');
		$this->mpdf->SetFooter('Tercetak {DATE j-m-Y H:i:s} |Halaman {PAGENO} / {nb}| ');

		$this->mpdf->setTitle($judul);
        // if (!empty($judul)) $this->mpdf->writeHTML($judul);
        $this->mpdf->writeHTML($isi);

        // $this->mpdf->Output();
		$this->mpdf->output($jdlsave,'I');

    }

	function kop($unit='')
    {

        $sql="SELECT*FROM tbl_namers where koders='$unit'";
			$query1    = $this->db->query($sql);

			$lcno        = 0;$koders        = 0;$namars        = 0;$alamat        = 0;$kota          = 0;$phone         = 0;$whatsapp      = 0;$fax           = 0;$email         = 0;$website       = 0;$pejabat1      = 0;$jabatan1      = 0;$ketbank       = 0;$ketbank2      = 0;$ketbank3      = 0;$pejabat2      = 0;$jabatan2      = 0;$jabatan3      = 0;$pejabat3      = 0;$jabatan4      = 0;$pejabat4      = 0;$namaapotik    = 0;$apoteker      = 0;$jabatan       = 0;$noijin        = 0;$npwp          = 0;$pkpno         = 0;$tglpkp        = 0;$wahost        = 0;$watoken       = 0;$mou           = 0;$tgl_awal      = 0;$tgl_akhir     = 0;

			foreach ($query1->result() as $row) {
				$lcno       = $lcno + 1;
				$koders     = $row->koders;
				$namars     = $row->namars;
				$alamat     = $row->alamat;
				$alamat2    = $row->alamat2;
				$kota       = $row->kota;
				$phone      = $row->phone;
				$whatsapp   = $row->whatsapp;
				$fax        = $row->fax;
				$email      = $row->email;
				$website    = $row->website;
				$pejabat1   = $row->pejabat1;
				$jabatan1   = $row->jabatan1;
				$ketbank    = $row->ketbank;
				$ketbank2   = $row->ketbank2;
				$ketbank3   = $row->ketbank3;
				$pejabat2   = $row->pejabat2;
				$jabatan2   = $row->jabatan2;
				$jabatan3   = $row->jabatan3;
				$pejabat3   = $row->pejabat3;
				$jabatan4   = $row->jabatan4;
				$pejabat4   = $row->pejabat4;
				$namaapotik = $row->namaapotik;
				$apoteker   = $row->apoteker;
				$jabatan    = $row->jabatan;
				$noijin     = $row->noijin;
				$npwp       = $row->npwp;
				$pkpno      = $row->pkpno;
				$tglpkp     = $row->tglpkp;
				$wahost     = $row->wahost;
				$watoken    = $row->watoken;
				$mou        = $row->mou;
				$tgl_awal   = $row->tgl_awal;
				$tgl_akhir  = $row->tgl_akhir;

			}
			$hasil =  array(
					   'namars'    => $namars,
					   'alamat'    => $alamat,
					   'alamat2'   => $alamat2,
					   'kota'      => $kota,
					   'phone'     => $phone,
					   'whatsapp'  => $whatsapp,
					   'npwp'      => $npwp
					);
			return $hasil;
    }

	
	
	
	
	
	


}
