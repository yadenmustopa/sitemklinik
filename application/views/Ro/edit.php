<?php
$this->load->view('template/header');
$this->load->view('template/body');
// var_dump( $tindakan );
// die();
?>

<style>

</style>

<!-- Modal -->
<div class="modal fade" id="tesModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal"><span>&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Tambah Billing</h4>
            </div>
            <form action="<?= base_url() ?>lab/saveBilling" method="POST">
                <input id="tarif_rs" type="hidden" value="0" class="form-control" name="tarif_rs" placeholder="" required>
                <input id="tarif_dr" type="hidden" value="0" class="form-control" name="tarif_dr" placeholder="" required>
                <input id="nolaborat" type="hidden" value="<?= $row->nolaborat ?>" class="form-control" name="nolaborat" placeholder="" required>
                <input id="id" type="hidden" value="<?= $row->id ?>" class="form-control" name="id" placeholder="" required>

                <div class="modal-body" style="padding-right: 30px;padding-left: 30px;">

                    <div class="form-group row">
                        <label for="namapas" class="col-sm-3 col-form-label">Tindakan</label>
                        <div class="col-sm-9">
                            <select id="tindakan_id" required name="tindakan_id" class="selectpicker" data-live-search="true" data-width="100%" onkeypress="return tabE(this,event)" required>
                                <option value="" selected disabled>-- Pilih Data --</option>
                                <?php
                                foreach ($tindakan as $value) {
                                ?>

                                    <option value="<?= $value->kodetarif ?>"><?= $value->tindakan ?></option>
                                <?php }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="qty" class="col-sm-3 col-form-label">QTY</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="qty" value="1" name="qty" placeholder="" readonly="" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tarifrp" class="col-sm-3 col-form-label">Tarif Rp</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <span class="input-group-addon">Rp</span>
                                <input id="tarifrp" type="text" class="form-control" name="tarifrp" placeholder="" readonly required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="namapas" class="col-sm-3 col-form-label">Cito</label>
                        <div class="col-sm-9">
                            <input type="checkbox" value="1" name="cito" class="form-check-inpu">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="citorp" class="col-sm-3 col-form-label">Cito Rp</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <span class="input-group-addon">Rp</span>
                                <input id="citorp" type="text" value="0" class="form-control" name="citorp" placeholder="" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="namapas" class="col-sm-3 col-form-label">Total Biaya</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <span class="input-group-addon">Rp</span>
                                <input id="total_biaya" type="text" class="form-control" name="total_biaya" placeholder="" readonly required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Tutup</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Edit -->
<div class="modal fade" id="tesModalEdit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal"><span>&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Billing</h4>
            </div>
            <form action="<?= base_url() ?>lab/editBilling" method="POST">
                <input id="tarif_rs" type="hidden" value="" class="form-control" name="tarif_rs" placeholder="" required>
                <input id="tarif_dr" type="hidden" value="" class="form-control" name="tarif_dr" placeholder="" required>
                <input id="nolaborat" type="hidden" value="<?= $row->nolaborat ?>" class="form-control" name="nolaborat" placeholder="" required>
                <input id="id" type="hidden" value="<?= $row->id ?>" class="form-control" name="id" placeholder="" required>

                <div class="modal-body" style="padding-right: 30px;padding-left: 30px;">

                    <div class="form-group row">
                        <label for="namapas" class="col-sm-3 col-form-label">Tindakan</label>
                        <div class="col-sm-9">
                            <select id="tindakan_id" required name="tindakan_id" class="selectpicker" data-live-search="true" data-width="100%" onkeypress="return tabE(this,event)" required>
                                <option value="" selected disabled>-- Pilih Data --</option>
                                <?php
                                foreach ($tindakan as $value) { ?>
                                    <option value="<?= $value->kodetarif ?>"><?= $value->tindakan ?></option>
                                <?php }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="qty" class="col-sm-3 col-form-label">QTY</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="qty" value="" name="qty" placeholder="" readonly="" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tarifrp" class="col-sm-3 col-form-label">Tarif Rp</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <span class="input-group-addon">Rp</span>
                                <input id="tarifrp" type="text" class="form-control" name="tarifrp" placeholder="" readonly required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="namapas" class="col-sm-3 col-form-label">Cito</label>
                        <div class="col-sm-9">
                            <input type="checkbox" value="1" name="cito" id="cito" class="form-check-inpu">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="citorp" class="col-sm-3 col-form-label">Cito Rp</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <span class="input-group-addon">Rp</span>
                                <input id="citorp" type="text" value="" class="form-control" name="citorp" placeholder="" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="namapas" class="col-sm-3 col-form-label">Total Biaya</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <span class="input-group-addon">Rp</span>
                                <input id="total_biaya" type="text" class="form-control" name="total_biaya" placeholder="" readonly required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Tutup</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                <span class="title-unit">
                    &nbsp;<?= $this->session->userdata("unit"); ?>
                </span>&nbsp;
                -
                &nbsp;<span class="title-web"><?= $menu; ?> <small> <?= $title; ?></small>
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li><i class="fa fa-home" style="color:#fff"></i>&nbsp;<a href="<?php echo base_url(); ?>dashboard" class="title-white">Awal</a>&nbsp;<i class="fa fa-angle-right" style="color:#fff"></i></li>
                <li><a href="#" class="title-white"><?= $menu; ?> </a></a>&nbsp;<i class="fa fa-angle-right" style="color:#fff"></i></li>
                <li><a href="#" class="title-white"><?= $title; ?> </a></a></li>
            </ul>
        </div>
    </div>

    <div class="row">

        <div class="col-md-12">
            <form action="<?= base_url() ?>lab/updateDataPemeriksaan" method="POST">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">ENTRY PEMERIKSAAN dan BILLING RADIOLOGI</div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="nolaborat" class="col-sm-3 col-form-label">No Pemeriksaan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="nolaborat" value="<?= $row->nolaborat ?>" id="nolaborat" placeholder="" readonly>
                                <input type="hidden" class="form-control" name="id" value="<?= $row->id ?>" id="id" placeholder="" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tgllab" class="col-sm-3 col-form-label">Tanggal Periksa</label>
                            <div class="col-sm-9">
                                <input type="date" class="form-control" id="tgllab" name="tgllab" value="<?= $row->tgllab ?>" placeholder="" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="noreg" class="col-sm-3 col-form-label">No Registrasi</label>
                            <div class="col-sm-5">
                                <select id="noreg" name="noreg" class="selectpicker" data-live-search="true" data-width="100%" onkeypress="return tabE(this,event)" required>
                                    <option value="" selected disabled>-- Pilih Data --</option>
                                    <?php
                                    foreach ($noReg as $value) { ?>
                                        <?php if ($row->noreg == $value->noreg) { ?>
                                            <option value="<?php echo $value->noreg ?>" selected><?php echo $value->noreg ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $value->noreg ?>"><?= $value->noreg ?></option>
                                        <?php } ?>
                                    <?php } ?>



                                </select>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="rekmed" name="rekmed" value="<?= $row->rekmed ?>" placeholder="No RM" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="namapas" class="col-sm-3 col-form-label">Nama Pasien</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="namapas" name="namapas" value="<?= $row->namapas ?>" placeholder="" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tgllahir" class="col-sm-3 col-form-label">Tanggal Lahir</label>
                            <div class="col-sm-9">
                                <input type="date" class="form-control" id="tgllahir" name="tgllahir" value="<?= $row->tgllahir ?>" placeholder="" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="umurth" class="col-sm-3 col-form-label">Umur</label>
                            <div class="col-sm-3">
                                <input type="number" class="form-control" id="umurth" name="umurth" value="<?= $row->umurth ?>" placeholder="Tahun" required>
                            </div>
                            <div class="col-sm-3">
                                <input type="number" class="form-control" id="umurbl" name="umurbl" value="<?= $row->umurbl ?>" placeholder="Bulan" required>
                            </div>
                            <div class="col-sm-3">
                                <input type="number" class="form-control" id="umurhr" name="umurhr" value="<?= $row->umurhr ?>" placeholder="Hari" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Jenis Kalamin</label>
                            <div class="col-sm-2 " style="margin-top: -10px !important;">
                                <input type="radio" class="form-check-input" value="1" name="jkel" id="jenis_kelamin1" placeholder="" required <?php echo $row->jkel == '1' ? 'checked' : 'null' ?>> <label for="jenis_kelamin1" style="margin-left: 20px;">Pria</label>
                            </div>
                            <div class="col-sm-2" style="margin-top: -10px !important;">
                                <input type="radio" class="form-check-input" value="2" name="jkel" id="jenis_kelamin2" placeholder="" required <?php echo $row->jkel == '2' ? 'checked' : 'null' ?>> <label for="jenis_kelamin2" style="margin-left: 20px;">Wanita</label>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="orderno" class="col-sm-3 col-form-label">No Order</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="orderno" value="<?= $row->orderno ?>" name="orderno" placeholder="" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Jenis Pasien</label>
                            <div class="col-sm-3" style="margin-top: -10px !important;">
                                <input required type="radio" class="form-check-input" value="1" id="jpas1" name="jpas" placeholder="" <?php echo $row->jpas == '1' ? 'checked' : 'null' ?>> <label for="jpas1" style="margin-left: 20px;">Pasien RS</label>
                            </div>
                            <div class="col-sm-3" style="margin-top: -10px !important;">
                                <input required type="radio" class="form-check-input" value="2" id="jpas2" name="jpas" placeholder="" <?php echo $row->jpas == '2' ? 'checked' : 'null' ?>> <label for="jpas2" style="margin-left: 20px;">Pasien Luar</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Jenis Pemeriksaan</label>
                            <div class="col-sm-3 " style="margin-top: -10px !important;">
                                <input type="radio" required class="form-check-input" value="1" id="jenisperiksa1" name="jenisperiksa" placeholder="" <?php echo $row->jenisperiksa == '1' ? 'checked' : 'null' ?>> <label for="jenisperiksa1" style="margin-left: 20px;">Rujukan Dokter</label>
                            </div>
                            <div class="col-sm-3" style="margin-top: -10px !important;">
                                <input type="radio" required class="form-check-input" value="2" id="jenisperiksa2" name="jenisperiksa" placeholder="" <?php echo $row->jenisperiksa == '2' ? 'checked' : 'null' ?>> <label for="jenisperiksa2" style="margin-left: 20px;">Permintaan Sendiri</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Pemeriksaan</label>
                            <div class="col-sm-3 " style="margin-top: -10px !important;">
                                <input type="radio" required class="form-check-input" value="1" id="rujuk1" name="rujuk" placeholder="" <?php echo $row->rujuk == '1' ? 'checked' : 'null' ?>> <label for="rujuk1" style="margin-left: 20px;">Lab Dalam</label>
                            </div>
                            <div class="col-sm-3" style="margin-top: -10px !important;">
                                <input type="radio" required class="form-check-input" value="2" id="rujuk2" name="rujuk" placeholder="" <?php echo $row->rujuk == '2' ? 'checked' : 'null' ?>> <label for="rujuk2" style="margin-left: 20px;">Dikirim Ke Faskes Lain</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="diagnosa" class="col-sm-3 col-form-label">Diagnosa</label>
                            <div class="col-sm-9">
                                <input type="text" required class="form-control" id="diagnosa" name="diagnosa" value="<?= $row->diagnosa ?>" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary" style="float: right;"><i class="fa fa-floppy-o" aria-hidden="true"></i> Update</button>
                        <a href="<?= base_url() ?>lab" class="btn btn-warning" style="float: right;margin-right:4px !important"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
            </form>

            <div>
                <div class="mb-5">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Billing</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">BHP</a></li>
                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Hasil</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div class="col-md-8">
                                <button class="btn btn-primary" style="margin-bottom: 10px;" data-toggle="modal" data-target="#tesModal"><i class="fa fa-plus" aria-hidden="true"></i> Tambah Billing</button>
                                <table class="table table-bordered" id="dynamic_field">
                                    <thead bgcolor="#00c11e">
                                        <tr style="color: white;">
                                            <th scope="col" style="width: 90px;">Aksi</th>
                                            <th scope="col">Tindakan</th>
                                            <th scope="col" style="width: 100px;">Qty</th>
                                            <th scope="col" style="width: 120px;">Tarif RP</th>
                                            <th scope="col" style="width: 100px;">Cito</th>
                                            <th scope="col" style="width: 120px;">Cito Rp</th>
                                            <th scope="col" style="width: 150px;">Total Biaya</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $unit = $this->session->userdata("unit");
                                        $dataBilling = $this->db->query("SELECT tbl_dlab.*,daftar_tarif_nonbedah.tindakan,daftar_tarif_nonbedah.koders from tbl_dlab
                                        join daftar_tarif_nonbedah on daftar_tarif_nonbedah.kodetarif = tbl_dlab.kodetarif
                                        where nolaborat='$row->nolaborat' and daftar_tarif_nonbedah.koders ='$unit'")->result();
                                        ?>
                                        <?php foreach ($dataBilling as $value) {

                                        ?>
                                            <tr>
                                                <td>
                                                    <a href="#" class="btn btn-primary btn-sm update_data" data-jenis="<?= $value->jenis ?>" data-tarifrs="<?= $value->tarifrs ?>" data-tarifdr="<?= $value->tarifdr ?>" data-qty="<?= $value->qty ?>" data-cito_rp="<?= $value->cito_rp ?>" data-total_biaya="<?= $value->total_biaya ?>" data-kode_tarif="<?= $value->kodetarif ?>" data-toggle="modal" data-target="#tesModalEdit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <a href="<?= base_url() ?>lab/delDataBilling/<?= $value->id ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </td>
                                                <td><?= $value->tindakan ?></td>
                                                <td><?= $value->qty ?></td>
                                                <td><?= $value->tarifrs +  $value->tarifdr ?></td>
                                                <?php if ($value->jenis == 1) { ?>
                                                    <td> <input type="checkbox" value="" name="" class="form-check-inpu" checked disabled style="color: blue;"></td>
                                                <?php } else { ?>
                                                    <td> <input type="checkbox" value="" name="" class="form-check-inpu" disabled></td>
                                                <?php } ?>

                                                <td><?= $value->cito_rp ?></td>
                                                <td><?= $value->total_biaya ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <th colspan="6" style="text-align: right;">Total Billing</th>
                                        <?php $grand_total = $this->db->query("SELECT SUM(total_biaya) AS grand_total FROM tbl_dlab where nolaborat='$row->nolaborat'")->row();
                                        ?>
                                        <th><?= $grand_total->grand_total ?></th>
                                    </tfoot>

                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div class="col-md-8">
                                <button class="btn btn-primary" style="margin-bottom: 10px;"><i class="fa fa-plus" aria-hidden="true"></i> Tambah Bhp</button>
                                <table class="table table-bordered">
                                    <thead bgcolor="#00c11e">
                                        <tr style="color: white;">
                                            <th scope="col">Aksi</th>
                                            <th scope="col">Bill</th>
                                            <th scope="col">Nama Barang</th>
                                            <th scope="col">Satuan</th>
                                            <th scope="col">Qty</th>
                                            <th scope="col">Harga</th>
                                            <th scope="col">Total Harga</th>
                                            <th scope="col">Lokasi Barang</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Mark</td>
                                            <td>Otto</td>
                                            <td>@mdo</td>
                                            <td>Mark</td>
                                            <td>Otto</td>
                                            <td>@mdo</td>
                                            <td>Otto</td>
                                            <td>@mdo</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="messages">
                            <form>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Tanggal Sample Di Ambil</label>
                                        <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Jam Sample Di Ambil</label>
                                        <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Oleh Petugas</label>
                                        <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Tanggal Selesai Periksa</label>
                                        <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Jam Selesai Periksa</label>
                                        <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                        <label class="form-check-label" for="exampleCheck1">Final Oleh</label>
                                    </div>
                                </div>
                                <!-- <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Tanggal Sample Di Ambil</label>
                                        <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
                                    </div>
                                </div> -->
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>



<?php
$this->load->view('template/footer');

?>

<script>
    $(document).ready(function() {
        $('#dataTableEx').DataTable();
    });
</script>

<script>
    $('#noreg').change(function() {
        let noreg = $(this).val()
        $('#rekmed').val('Loading...')
        $('#namapas').val('Loading...')
        $.ajax({
            url: '<?php echo site_url('lab/pasien_daftar') ?>',
            type: 'POST',
            dataType: "JSON",
            data: {
                noreg: noreg
            },
            success: function(res) {
                let data;
                try {
                    data = JSON.parse(res)
                } catch (e) {
                    data = res
                }
                $('#rekmed').val(data.rekmed)
                $('#namapas').val(data.namapas)
            }
        });
    })
</script>

<script>
    $('#tindakan_id').change(function() {
        let kodetarif = $(this).val()
        var citorp = $('#citorp').val()
        $('#tarifrp').val('Loading...')
        $('#total_biaya').val('Loading...')
        $.ajax({
            url: '<?php echo site_url('lab/get_daftar_tarif_nonbedah') ?>',
            type: 'POST',
            dataType: "JSON",
            data: {
                kodetarif: kodetarif
            },
            success: function(res) {
                let data;
                try {
                    data = JSON.parse(res)
                } catch (e) {
                    data = res
                }
                var jml = parseFloat(data.tarifrspoli) + parseFloat(data.tarifdrpoli)
                $('#tarifrp').val(parseFloat(jml))

                if (citorp) {
                    var citorp = citorp
                } else {
                    var citorp = 0
                }

                if (jml) {
                    var jml = jml
                } else {
                    var jml = 0
                }

                $('#total_biaya').val(parseFloat(jml) + parseFloat(citorp))
                $('#tarif_rs').val(parseFloat(data.tarifrspoli))
                $('#tarif_dr').val(parseFloat(data.tarifdrpoli))
            }
        });
    });


    $(document).on('keyup mouseup', '#citorp', function() {
        var citorp = $('#citorp').val()
        var jml = $('#tarifrp').val()
        if (citorp) {
            var citorp = citorp
        } else {
            var citorp = 0
        }

        if (jml) {
            var jml = jml
        } else {
            var jml = 0
        }
        $('#total_biaya').val(parseFloat(jml) + parseFloat(citorp))
    })
</script>

<script>
    $('.update_data').click(function() {
        var jenis = $(this).data('jenis');
        var qty = $(this).data('qty');
        var cito_rp = $(this).data('cito_rp');
        var total_biaya = $(this).data('total_biaya');
        var tarifrs = $(this).data('tarifrs');
        var tarifdr = $(this).data('tarifdr');
        var kode_tarif = $(this).data('kode_tarif');
        var tarifrp = tarifrs + tarifdr;

        if (jenis == 1) {
            $('#tesModalEdit #cito').prop("checked", true);
        } else {
            $('#tesModalEdit #cito').prop("checked", false);
        }
        $('#tesModalEdit #qty').val(qty);
        $('#tesModalEdit #citorp').val(cito_rp);
        $('#tesModalEdit #total_biaya').val(total_biaya);
        $('#tesModalEdit #tarif_rs').val(tarifrs);
        $('#tesModalEdit #tarif_dr').val(tarifdr);
        $('#tesModalEdit #tarifrp').val(tarifrp);
        $('#tesModalEdit #tindakan_id').val(kode_tarif).change();

    });
</script>