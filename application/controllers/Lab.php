<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Lab extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->session->set_userdata('menuapp', '2000');
		$this->session->set_userdata('submenuapp', '2651');
		$this->load->model(array("M_bedah", "M_pasien_global", "M_cetak", "M_lab", "M_barang", "M_alkes_transaksi"));
		$this->load->helper(array("app_global", "rsreport"));
	}

	public function index()
	{
		$data = [
			'title'				=> 'Labolatorium',
			'menu'				=> 'Labolatorium',
			'orderUnit'			=> $this->M_lab->get_all(),

		];

		$this->load->view('Lab/index', $data);
	}

	public function getDataPemeriksaan()
	{
		// $columns = array('nolaborat', 'noreg');
		// $queryData = "SELECT tbl_hlab.*,tbl_dokter.nadokter,tbl_tarifh.tindakan FROM tbl_hlab
		// join tbl_dokter on tbl_dokter.kodokter =tbl_hlab.drpengirim 
		// left join tbl_dlab on tbl_dlab.nolaborat =tbl_hlab.nolaborat 
		// left join tbl_tarifh on tbl_tarifh.kodetarif =tbl_dlab.kodetarif WHERE";
		$queryData = "SELECT tbl_hlab.*,tbl_dokter.nadokter FROM tbl_hlab
		join tbl_dokter on tbl_dokter.kodokter =tbl_hlab.drpengirim WHERE";
		if ($_POST["is_date_search"] == "yes") {
			$dateNowStart = $_POST["start_date"] . ' 00:00:00';
			$dateNowEns = $_POST["end_date"] . ' 23:59:59';
			$queryData .= ' tgllab >= "' . $dateNowStart . '" and tgllab  <= "' . $dateNowEns . '" AND ';
		} else {
			$dateNowStart = date('Y-m-d') . ' 00:00:00';
			$dateNowEns = date('Y-m-d') . ' 23:59:59';
			$queryData .= ' tgllab >= "' . $dateNowStart . '" and tgllab  <= "' . $dateNowEns . '" AND ';
		}

		if (isset($_POST["search"]["value"])) {
			$queryData .= '
			(tbl_hlab.nolaborat LIKE "%' . $_POST["search"]["value"] . '%" 
			OR noreg LIKE "%' . $_POST["search"]["value"] . '%" 
			OR rekmed LIKE "%' . $_POST["search"]["value"] . '%" 
			OR namapas LIKE "%' . $_POST["search"]["value"] . '%")
			';
		}


		if (isset($_POST["order"])) {
			// $queryData .= ' ORDER BY ' . $columns[$_POST['order']['0']['column']] . ' ' . $_POST['order']['0']['dir'] . '';
		} else {
			$queryData .= ' ORDER BY tbl_hlab.id DESC ';
		}

		$queryData1 = '';

		if ($_POST["length"] != -1) {
			$queryData1 = ' LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
		}

		$result = $this->db->query($queryData . $queryData1);
		$unit = $this->session->userdata("unit");
		$data = array();
		foreach ($result->result() as $value) {
			$sub_array = array();
			$sub_array[] = '<a href="' . base_url() . 'lab/getData/' . $value->id . '" class="btn green btn-sm">Edit</a>';
			$sub_array[] = '<button class="btn green btn-sm">Isi</button> <button class="btn green btn-sm">Serahkan</button>';
			$sub_array[] = $value->nolaborat;
			$sub_array[] = $value->noreg;
			$sub_array[] = substr($value->tgllab, 0, 10) . ' ' . $value->jam;
			$sub_array[] = $value->rekmed;
			$sub_array[] = $value->namapas;
			$dataBilling = $this->db->query("SELECT tbl_dlab.*,daftar_tarif_nonbedah.tindakan,daftar_tarif_nonbedah.koders from tbl_dlab
			join daftar_tarif_nonbedah on daftar_tarif_nonbedah.kodetarif = tbl_dlab.kodetarif
			where nolaborat='$value->nolaborat' and daftar_tarif_nonbedah.koders ='$unit'")->result();
			$sub_array[] = $this->listArr($dataBilling);
			$sub_array[] = $value->nadokter;
			$data[] = $sub_array;
		}

		$output = array(
			"draw"    => intval($_POST["draw"]),
			"recordsTotal"  =>  $this->count_all($dateNowStart, $dateNowEns),
			"recordsFiltered" => $this->number_filter_row($queryData),
			"data"    => $data
		);

		echo json_encode($output);
	}

	function listArr($arr) {
		$html = '<ul>';
		foreach ($arr as $item) {
				$html .= '<li>' . $item->tindakan . '</li>';
		}
		$html .= '</ul>';
		return $html;
	}

	function count_all($starDate, $endDate)
	{
		$this->db->from("tbl_hlab");
		$this->db->where('tgllab >=', $starDate);
		$this->db->where('tgllab <=', $endDate);
		return $this->db->count_all_results();
	}
	function number_filter_row($queryData)
	{
		$query = $this->db->query($queryData);
		return $query->num_rows();
	}

	public function addDataPemeriksaan()
	{
		$data = [
			'title'				=> 'Create',
			'menu'				=> 'Tambah Pemeriksaan',
			'kodeNolaborat'		=> $this->M_lab->generatekode(),
			'noReg'				=> $this->M_lab->noReg(),
			'dataDokter'		=> $this->M_lab->dataDokter(),
			'petugas'			=> $this->M_lab->dataPetugas(),
		];
	
		$this->load->view('Lab/create', $data);
	}

	public function simpanDataPemeriksaan()
	{
		$fieldData = array(
			'nolaborat' => $this->input->post('nolaborat', TRUE),
			'tgllab' => $this->input->post('tgllab', TRUE),
			'noreg' => $this->input->post('noreg', TRUE),
			'namapas' => $this->input->post('namapas', TRUE),
			'rekmed' => $this->input->post('rekmed', TRUE),
			'tgllahir' => $this->input->post('tgllahir', TRUE),
			'umurth' => $this->input->post('umurth', TRUE),
			'umurbl' => $this->input->post('umurbl', TRUE),
			'umurhr' => $this->input->post('umurhr', TRUE),
			'jkel' => $this->input->post('jkel', TRUE),
			'orderno' => $this->input->post('orderno', TRUE),
			'jpas' => $this->input->post('jpas', TRUE),
			'jenisperiksa' => $this->input->post('jenisperiksa', TRUE),
			'rujuk' => $this->input->post('rujuk', TRUE),
			'diagnosa' => $this->input->post('diagnosa', TRUE),
			'drperiksa' => $this->input->post('drperiksa', TRUE),
			'drpengirim' => $this->input->post('drpengirim', TRUE),
			'kodepetugas' => $this->input->post('kodepetugas', TRUE),
			'username' => $this->session->userdata('username'),
			'jam' => date('H:i:s'),

		);
		$this->M_lab->insert($fieldData);
		$id = $this->db->insert_id();
		redirect('lab/getData/' . $id);
	}

	public function updateDataPemeriksaan()
	{
		$fieldData = array(
			'nolaborat' => $this->input->post('nolaborat', TRUE),
			'tgllab' => $this->input->post('tgllab', TRUE),
			'noreg' => $this->input->post('noreg', TRUE),
			'namapas' => $this->input->post('namapas', TRUE),
			'rekmed' => $this->input->post('rekmed', TRUE),
			'tgllahir' => $this->input->post('tgllahir', TRUE),
			'umurth' => $this->input->post('umurth', TRUE),
			'umurbl' => $this->input->post('umurbl', TRUE),
			'umurhr' => $this->input->post('umurhr', TRUE),
			'jkel' => $this->input->post('jkel', TRUE),
			'orderno' => $this->input->post('orderno', TRUE),
			'jpas' => $this->input->post('jpas', TRUE),
			'jenisperiksa' => $this->input->post('jenisperiksa', TRUE),
			'rujuk' => $this->input->post('rujuk', TRUE),
			'diagnosa' => $this->input->post('diagnosa', TRUE),
			'drperiksa' => $this->input->post('drperiksa', TRUE),
			'drpengirim' => $this->input->post('drpengirim', TRUE),
			'kodepetugas' => $this->input->post('kodepetugas', TRUE),
			// 'username' => $this->session->userdata('username'),
			'jam' => date('H:i:s'),
			'editby' => $this->session->userdata('username'),
			'tgledit' => date('Y-m-d'),
		);
		$this->M_lab->update($this->input->post('id'), $fieldData);
		redirect('lab/getData/' . $this->input->post('id'));
	}

	public function getData($id)
	{
		$data = [
			'title'				=> 'Edit ',
			'menu'				=> 'Edit Pemeriksaan',
			'row'				=> $this->M_lab->get_by_id($id),
			'noReg'				=> $this->M_lab->noReg(),
			'dataDokter'		=> $this->M_lab->dataDokter(),
			'tindakan'			=> $this->M_lab->get_tindakan(),
			'petugas'			=> $this->M_lab->dataPetugas(),
			'barang'			=> $this->M_barang->getAll(),
			// 'alkes_transaksi'   => $this->M_alkes_transaksi->getByNotr(),
		];
		$this->load->view('Lab/edit', $data);
	}

	public function pasien_daftar()
	{
		$noreg = $this->input->post('noreg');
		$query = $this->db->query("SELECT * from pasien_daftar where noreg='$noreg'")->row();
		echo json_encode($query);
	}

	public function  get_daftar_tarif_nonbedah()
	{
		$kodetarif = $this->input->post('kodetarif');
		$query = $this->db->query("SELECT * from daftar_tarif_nonbedah where kodetarif='$kodetarif'")->row();
		echo json_encode($query);
	}

	public function  get_pemeriksaan()
	{
		$nolaborat = $this->input->post('nolaborat');
		$dlab_datas = $this->db->query("SELECT * from tbl_dlab where nolaborat='$nolaborat'")->results();
		$tarif_datas = [];
		$labmashasil_datas = [];
		foreach ($dlab_datas as $value) {
			$kode_tarif = $value->kodetarif;
			$tarif_datas =  $this->db->query("SELECT * from tbl_tarifrs where kodetarif='$kode_tarif'")->results();
		}

		foreach ($tarif_datas as  $value) {
			$labmashasil_datas =  $this->db->query("SELECT * from tbl_tarif where kodetarif='$kode_tarif'")->results();
		}

		echo json_encode($labmashasil_datas);
	}

	

	public function saveBilling()
	{
		$cito = $this->input->post('cito', TRUE);

		if ($cito) {
			$jenis = 1;
		} else {
			$jenis = 0;
		}
		$data = array(
			'nolaborat' => $this->input->post('nolaborat', TRUE),
			'kodetarif' => $this->input->post('tindakan_id', TRUE),
			'qty' => $this->input->post('qty', TRUE),
			'tarifrs' => $this->input->post('tarif_rs', TRUE),
			'tarifdr' => $this->input->post('tarif_dr', TRUE),
			'jenis' => $jenis,
			'cito_rp' => $this->input->post('citorp', TRUE),
			'total_biaya' => $this->input->post('total_biaya', TRUE),
		);
		$this->db->insert('tbl_dlab', $data);
		redirect('lab/getData/' . $this->input->post('id'));
	}

	public function saveBhp()
	{
		$dibebankan = ( $this->input->post('bill', TRUE ) === 'on' ) ? 1 : 0;

		$data = array(
			'notr' => $this->input->post('notr', TRUE),
			'koders' => $this->input->post('koders', TRUE),
			'dibebankan' => $dibebankan,
			'kodeobat' => $this->input->post('kodeobat', TRUE),
			'satuan' => $this->input->post('satuan', TRUE),
			'qty' => $this->input->post('qty', TRUE),
			'harga' => $this->input->post('harga', TRUE ),
			'totalharga' => $this->input->post('totalharga', TRUE),
			'gudang' => $this->input->post('gudang', TRUE),
			'tgltransaksi' => date("Y-m-d H:i:s")
		);

		
		$this->db->insert('tbl_alkestransaksi', $data);
		redirect('lab/getData/' . $this->input->post('id'));
	}


	public function updateBilling()
	{
		$cito = $this->input->post('cito', TRUE);

		if ($cito) {
			$jenis = 1;
		} else {
			$jenis = 0;
		}

		$data = array(
			'nolaborat' => $this->input->post('nolaborat', TRUE),
			'kodetarif' => $this->input->post('tindakan_id', TRUE),
			'qty' => $this->input->post('qty', TRUE),
			'tarifrs' => $this->input->post('tarif_rs', TRUE),
			'tarifdr' => $this->input->post('tarif_dr', TRUE),
			'jenis' => $jenis,
			'cito_rp' => $this->input->post('citorp', TRUE),
			'total_biaya' => $this->input->post('total_biaya', TRUE),
		);
		$this->db->where('id',$this->input->post('id_billing'));
		$this->db->update('tbl_dlab', $data);
		redirect('lab/getData/' . $this->input->post('id'));
	}

	public function updateBhp()
	{


		$dibebankan = ( $this->input->post('bill', TRUE ) === 'on' ) ? 1 : 0;

		$data = array(
			'notr' => $this->input->post('notr', TRUE),
			'koders' => $this->input->post('koders', TRUE),
			'dibebankan' => $dibebankan,
			'kodeobat' => $this->input->post('kodeobat', TRUE),
			'satuan' => $this->input->post('satuan', TRUE),
			'qty' => $this->input->post('qty', TRUE),
			'harga' => $this->input->post('harga', TRUE ),
			'totalharga' => $this->input->post('totalharga', TRUE),
			'gudang' => $this->input->post('gudang', TRUE),
		);

	
		$this->db->where('id',$this->input->post('id'));
		$this->db->update('tbl_alkestransaksi', $data);
		redirect('lab/getData/' . $this->input->post('id_pemeriksaan'));
	}

	public function delDataBilling($id){
		$this->db->where('id', $id);
        $this->db->delete('tbl_dlab');
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function delDataBhp($id){
		$this->db->where('id', $id);
        $this->db->delete('tbl_alkestransaksi');
		redirect($_SERVER['HTTP_REFERER']);
	}
}
