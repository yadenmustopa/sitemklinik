<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ro extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		// $this->session->set_userdata('menuapp', '2000');
		// $this->session->set_userdata('submenuapp', '2651');
		$this->load->model(array("M_ro"));
		$this->load->helper(array("app_global", "rsreport"));
	}

	public function index()
	{
		$data = [
			'title'				=> 'Radiologi',
			'menu'				=> 'Radiologi',
			// 'orderUnit'			=> $this->M_ro->get_all(),

		];

		$this->load->view('Ro/index', $data);
	}

	public function getDataPemeriksaan()
	{
		// $columns = array('nolaborat', 'noreg');
		// $queryData = "SELECT tbl_hlab.*,tbl_dokter.nadokter,tbl_tarifh.tindakan FROM tbl_hlab
		// join tbl_dokter on tbl_dokter.kodokter =tbl_hlab.drpengirim 
		// left join tbl_dlab on tbl_dlab.nolaborat =tbl_hlab.nolaborat 
		// left join tbl_tarifh on tbl_tarifh.kodetarif =tbl_dlab.kodetarif WHERE";
		$queryData = "SELECT tbl_hlab.*,tbl_dokter.nadokter FROM tbl_hlab
		join tbl_dokter on tbl_dokter.kodokter =tbl_hlab.drpengirim WHERE";
		if ($_POST["is_date_search"] == "yes") {
			$dateNowStart = $_POST["start_date"] . ' 00:00:00';
			$dateNowEns = $_POST["end_date"] . ' 23:59:59';
			$queryData .= ' tgllab >= "' . $dateNowStart . '" and tgllab  <= "' . $dateNowEns . '" AND ';
		} else {
			$dateNowStart = date('Y-m-d') . ' 00:00:00';
			$dateNowEns = date('Y-m-d') . ' 23:59:59';
			$queryData .= ' tgllab >= "' . $dateNowStart . '" and tgllab  <= "' . $dateNowEns . '" AND ';
		}

		if (isset($_POST["search"]["value"])) {
			$queryData .= '
			(tbl_hlab.nolaborat LIKE "%' . $_POST["search"]["value"] . '%" 
			OR noreg LIKE "%' . $_POST["search"]["value"] . '%" 
			OR rekmed LIKE "%' . $_POST["search"]["value"] . '%" 
			OR namapas LIKE "%' . $_POST["search"]["value"] . '%")
			';
		}


		if (isset($_POST["order"])) {
			// $queryData .= ' ORDER BY ' . $columns[$_POST['order']['0']['column']] . ' ' . $_POST['order']['0']['dir'] . '';
		} else {
			$queryData .= ' ORDER BY tbl_hlab.id DESC ';
		}

		$queryData1 = '';

		if ($_POST["length"] != -1) {
			$queryData1 = ' LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
		}

		$result = $this->db->query($queryData . $queryData1);
		$unit = $this->session->userdata("unit");
		$data = array();
		foreach ($result->result() as $value) {
			$sub_array = array();
			$sub_array[] = '<a href="' . base_url() . 'ro/getData/' . $value->id . '" class="btn green btn-sm">Edit</a>';
			$sub_array[] = '<button class="btn green btn-sm">Isi</button> <button class="btn green btn-sm">Serahkan</button>';
			$sub_array[] = $value->nolaborat;
			$sub_array[] = $value->noreg;
			$sub_array[] = substr($value->tgllab, 0, 10) . ' ' . $value->jam;
			$sub_array[] = $value->rekmed;
			$sub_array[] = $value->namapas;
			$dataBilling = $this->db->query("SELECT tbl_dlab.*,daftar_tarif_nonbedah.tindakan,daftar_tarif_nonbedah.koders from tbl_dlab
			join daftar_tarif_nonbedah on daftar_tarif_nonbedah.kodetarif = tbl_dlab.kodetarif
			where nolaborat='$value->nolaborat' and daftar_tarif_nonbedah.koders ='$unit'")->result();
			$sub_array[] = $this->listArr($dataBilling);
			$sub_array[] = $value->nadokter;
			$data[] = $sub_array;
		}

		$output = array(
			"draw"    => intval($_POST["draw"]),
			"recordsTotal"  =>  $this->count_all($dateNowStart, $dateNowEns),
			"recordsFiltered" => $this->number_filter_row($queryData),
			"data"    => $data
		);

		echo json_encode($output);
	}

	function listArr($arr) {
		$html = '<ul>';
		foreach ($arr as $item) {
				$html .= '<li>' . $item->tindakan . '</li>';
		}
		$html .= '</ul>';
		return $html;
	}

	function count_all($starDate, $endDate)
	{
		$this->db->from("tbl_hlab");
		$this->db->where('tgllab >=', $starDate);
		$this->db->where('tgllab <=', $endDate);
		return $this->db->count_all_results();
	}
	function number_filter_row($queryData)
	{
		$query = $this->db->query($queryData);
		return $query->num_rows();
	}


	public function addDataPemeriksaan()
	{
		$data = [
			'title'				=> 'Create',
			'menu'				=> 'Tambah Pemeriksaan',
			'kodeNolaborat'		=> $this->M_ro->generatekode(),
			'noReg'				=> $this->M_ro->noReg(),

		];
	
		$this->load->view('Ro/create', $data);
	}

	public function getData($id)
	{
		$data = [
			'title'				=> 'Edit ',
			'menu'				=> 'Edit Pemeriksaan',
			'row'				=> $this->M_ro->get_by_id($id),
			'noReg'				=> $this->M_ro->noReg(),
			'tindakan'			=> $this->M_ro->get_tindakan(),
		];
		$this->load->view('Ro/edit', $data);
	}
}
